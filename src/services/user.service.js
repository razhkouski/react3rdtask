import data from '../defaultStates/user.json';

export const getUsersQuery = () => {
    return fetch(`https://jsonplaceholder.typicode.com/users/1`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
};