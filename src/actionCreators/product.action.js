export const addProduct = (data) => ({type: 'ADD_PRODUCT', data});
export const editProduct = (data) => ({type: 'EDIT_PRODUCT', data});
export const deleteProduct = (id) => ({type: 'DELETE_PRODUCT', id});
export const findProduct = (data) => ({type: 'FIND_PRODUCT', data});
export const resetProducts = () => ({type: 'RESET_PRODUCTS'});