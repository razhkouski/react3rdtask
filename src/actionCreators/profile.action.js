export const editUser = (data) => ({ type: 'EDIT_USER', data});
export const fetchUserSuccess = (data) => ({type: 'FETCH_USER_SUCCESS', data});