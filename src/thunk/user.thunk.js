import { getUsersQuery } from "../services/user.service";
import { fetchUserSuccess } from "../actionCreators/profile.action";

export const getAllUsers = () => (dispatch) => {
    getUsersQuery()
        .then((response) => response.json())
        .then((data) => {
            dispatch(fetchUserSuccess(data));
        })
        .catch((err) => {
            console.log(err)
        })
};
