import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { store } from './store/store';
import { Provider } from 'react-redux';
import Navbar from './components/Navbar';
import Logo from './components/Logo';
import AdminPart from './components/AdminPart';
import UserPart from './components/UserPart';
import Profile from './components/Profile';

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Route path='/home' component={Logo} />
                <Route path='/adminPart' component={AdminPart} />
                <Route path='/userPart' component={UserPart} />
                <Route path='/profile' component={Profile} />
            </BrowserRouter>
        </Provider>
    )
}

export default App;