const defaultState = {};

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'EDIT_USER':
            let nState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };

            return nState;
        case 'FETCH_USER_SUCCESS':
            let newState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };
            
            return newState;
        default:
            return state;
    }
};