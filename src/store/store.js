import {createStore, combineReducers, applyMiddleware, Reducer} from "redux";
import thunk from "redux-thunk";
import logoReducer from "../reducers/logo.reducer";
import productReducer from '../reducers/product.reducer';
import userReducer from "../reducers/user.reducer";

export const rootReducer = combineReducers({
    logo: logoReducer,
    products: productReducer,
    user: userReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
