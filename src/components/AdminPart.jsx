import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
// import { getAllUsers } from "../thunk/user.thunk";
import { addProduct, deleteProduct, editProduct } from '../actionCreators/product.action';

function AdminPart(props) {
    const [id, setId] = useState();
    const [title, setTitle] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();
    const [isEdit, setIsEdit] = useState(false);

    const addProduct = () => {
        let product = {
            id: isEdit ? id : props.products.length + 1,
            title: title,
            img: isEdit ? props.products[id - 1].img : 'https://content2.onliner.by/catalog/device/header/98b65279323ea2beeba0c347f365f728.jpeg',
            description: description,
            price: price
        };

        isEdit ? props.editProduct(product) : props.addProduct(product);

        setTitle('');
        setDescription('');
        setPrice('');
        setIsEdit(false);
    };

    const handleEdit = (product) => {
        setIsEdit(true);
        setId(product.id);
        setTitle(product.title);
        setDescription(product.description);
        setPrice(product.price);
    };

    const handleDelete = (product) => {
        props.deleteProduct(product.id);
    };

    return (
        <>
            <h1>Product database</h1>
            Name: <input value={title} onChange={(e) => setTitle(e.target.value)} />
            Description: <input value={description} onChange={(e) => setDescription(e.target.value)} />
            Price: <input value={price} onChange={(e) => setPrice(e.target.value)} />
            <button onClick={addProduct}>Add</button>
            {
                props.products.map(product => (
                    <div key={product.id}>
                        <div>
                            <p>{product.title}</p>
                            <img src={product.img} alt="" /> 
                            <p>{product.description}</p>
                            <p>{product.price} BYN</p>
                        </div>
                        <button onClick={() => handleEdit(product)}>Edit</button>
                        <button onClick={() => handleDelete(product)}>Delete</button>
                    </div>
                ))
            }
        </>
    )
};

const mapStateToProps = (state) => ({
    products: state.products
});

const mapDispatchToProps = (dispatch) => ({
    addProduct: (data) => dispatch(addProduct(data)),
    editProduct: (data) => dispatch(editProduct(data)),
    deleteProduct: (id) => dispatch(deleteProduct(id))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminPart);
