import React, { useState } from 'react';
import { connect } from 'react-redux';

function Logo(props) {
    return (
        <>
            <main key={props.logo.id}>
                <div className="logoBlock">
                    <img src={props.logo.img} alt='' />
                    <h1>{props.logo.title}</h1>
                </div>
                <p>{props.logo.description}</p>
            </main>
        </>
    )
}

const mapStateToProps = (state) => ({
    logo: state.logo
});

export default connect(
    mapStateToProps
)(Logo)