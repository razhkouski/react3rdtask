import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { editUser, fetchUserSuccess } from '../actionCreators/profile.action';
import { getAllUsers } from '../thunk/user.thunk';

function Profile(props) {
    const [name, setName] = useState(props.user.name);
    const [surname, setSurname] = useState(props.user.surname);
    const [card, setCard] = useState(props.user.card);

    useEffect(() => {
        props.getUser();
    }, []);

    const handleEdit = () => {
        let user = {
            name: name ? name : props.user.name,
            surname: surname ? surname : props.user.surname,
            card: card ? card : props.user.card
        };

        props.editUser(user);

        setName('');
        setSurname('');
        setCard('');
    }

    return (
        <>
            <h1>User information</h1>
            <div key={props.user.id}>
                <p>{props.user.name} {props.user.surname}</p>
                <p>Card number: {props.user.card}</p>
            </div>
            Name: <input value={name} onChange={(e) => setName(e.target.value)}></input>
            Surname: <input value={surname} onChange={(e) => setSurname(e.target.value)}></input>
            Card number: <input value={card} onChange={(e) => setCard(e.target.value)}></input>
            <button onClick={() => handleEdit(props.user)}>Edit</button>
        </>
    )
};

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => dispatch(editUser(data)),
    getUser: () => dispatch(getAllUsers())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);